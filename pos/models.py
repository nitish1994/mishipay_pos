from django.db import models

# Create your models here.
#TODO: Can have Cart, CartItem and product models as well




class Product(models.Model):
    product_id = models.TextField(blank=True, null=True)
    title = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now=True)
    product_type = models.TextField(blank=True, null=True)


class Variant(models.Model):
    product = models.ForeignKey(Product, related_name="product_variant", on_delete=models.deletion.CASCADE)
    variant_id = models.TextField(blank=True, null=True)
    title = models.TextField(blank=True, null=True)
    price = models.DecimalField(max_digits=12, decimal_places=2, default=0)
    sku =  models.TextField(blank=True, null=True)
    inventory_quantity = models.FloatField()


class Order(models.Model):
    order_id = models.TextField(blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)
    completed_on = models.DateTimeField(blank=True, null=True)
    total_amount = models.DecimalField(max_digits=12, decimal_places=2, default=0)
    token = models.TextField(blank=True)


class OrderItem(models.Model):
    order = models.ForeignKey(Order, related_name="order_items", on_delete=models.deletion.CASCADE)
    item_id = models.TextField(blank=True, null=True)
    mrp = models.FloatField()
    quantity = models.FloatField()
