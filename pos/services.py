import requests
import base64

API_KEY = 'a38f4a6a8cb713fe2bebdbf3df331f54'
PASSWORD = '3182dcd29ff6c3f6f2dd325ba99b4216'
BASE_URL = 'https://mishipaytestdevelopmentemptystore.myshopify.com/admin/api/2020-07/'

def get_auth_token():
    key = API_KEY+':'+PASSWORD
    encoded = base64.b64encode(bytes(key, 'utf-8'))
    return encoded.decode('utf-8')


def get_product_details(product_id):
    url = BASE_URL+'products/{}.json'.format(product_id)
    headers = {
            'Authorization': 'Basic ' + get_auth_token()
        }

    try:
        resp = requests.get(url, headers=headers)
        if resp.status_code == 200 or resp.status_code == 404:
            return resp.json(), resp.status_code
        else:
            return None
    except:
        return None

def place_order(order_items):
    url = BASE_URL + 'orders.json'
    headers = {
        'Authorization': 'Basic ' + get_auth_token()
    }
    order = {}
    order['inventory_behaviour'] = 'decrement_obeying_policy'
    order['line_items'] = []
    for item in order_items:
        order['line_items'].append({
            "variant_id": item.get('item_id'),
            "quantity": item.get('quantity')
        })
    try:
        resp = requests.post(url, headers=headers, json={"order": order})
        if resp.status_code == 201:
            return resp.json()
        else:
            return None
    except:
        return None




