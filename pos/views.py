from django.shortcuts import render

# Create your views here.

from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from pos.services import get_product_details, place_order
from django.db import models, transaction, OperationalError
from pos.models import *

class Product(APIView):

    def get(self, request, **kwargs):
        product_info, status_code = get_product_details(kwargs.get('product_id'))
        if product_info:
            if status_code == 200:
                return Response(product_info, status.HTTP_200_OK)
            if status_code == 404:
                return Response(product_info, status.HTTP_404_NOT_FOUND)
        return Response({'message': 'Error while fetching products info'}, status.HTTP_406_NOT_ACCEPTABLE)

class Order(APIView):

    def queryset(self, ids):
        return Variant.objects.filter(variant_id__in=ids)

    def post(self, request):
        items = request.data.get('items')
        #order = place_order(items)
        variant_ids = []
        order = None
        for item in items:
            variant_ids.append(item.get('item_id'))
        print(items)
        with transaction.atomic():
            try:
                # Check inventory with DB Lock
                variants = list(self.queryset(variant_ids).select_for_update())
                for var in variants:
                    for item in items:
                        if str(var.variant_id) == str(item.get('item_id')):
                            if var.inventory_quantity < item.get('quantity'):
                                return Response({'message': 'Error while placing order. Insufficent stock'},
                                                status.HTTP_406_NOT_ACCEPTABLE)
                #Placing order
                order = place_order(items)
                #updating inventory
                for var in variants:
                    for item in items:
                        if str(var.variant_id) == str(item.get('item_id')):
                            var.inventory_quantity = var.inventory_quantity - item.get('quantity')
                            var.save()
            except:
                return Response({'message': 'Error while palcing order'}, status.HTTP_406_NOT_ACCEPTABLE)


        if order:
            return Response(order, status.HTTP_200_OK)

        return Response({'message': 'Error while palcing order'}, status.HTTP_406_NOT_ACCEPTABLE)



