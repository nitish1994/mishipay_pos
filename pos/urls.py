from django.urls import path
from pos.views import Product, Order
urlpatterns = [
    path('product/<int:product_id>', Product.as_view(), name='product-details'),
    path('order', Order.as_view(), name='order-creation'),
]