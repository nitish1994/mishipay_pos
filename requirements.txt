Django==3.0.8
djangorestframework==3.11.0
idna==2.10
mysqlclient==2.0.1
pytz==2020.1
requests==2.23.0
sqlparse==0.3.1
urllib3==1.25.9